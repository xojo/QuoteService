#tag Class
Protected Class App
Inherits WebApplication
	#tag Event
		Function HandleSpecialURL(Request As WebRequest) As Boolean
		  Select Case Request.Path
		  Case "quote"
		    // Select a random quote from the database
		    Dim sql As String = "SELECT QuoteText, QuoteSource FROM Quote ORDER BY RANDOM() LIMIT 1;"
		    Dim quoteRS As RecordSet = DB.SQLSelect(sql)
		    
		    Dim json As New JSONItem
		    If quoteRS <> Nil Then
		      json.Value("QuoteText") = quoteRS.Field("QuoteText").StringValue
		      json.Value("QuoteSource") = quoteRS.Field("QuoteSource").StringValue
		      quoteRS.Close
		    End If
		    
		    // Output the quote as JSON
		    Request.Print(json.ToString)
		    
		    // Return true to signal that the request was handled
		    Return True
		    
		  Case Else
		    // Output usage information
		    Return True
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Sub Open(args() as String)
		  // Connect to SQLite database on app start. This connection is
		  // used to process requests that come in to the service through
		  // HandleURL.
		  // Separate DB connections are used for UI sessions that allow
		  // submission of quotes.
		  Dim dbFile As FolderItem = GetDBFile
		  CreateDB(dbFile)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CreateDB(location As FolderItem)
		  // Create the DB at the specified location if it does not already
		  // exist, otherwise just connect to it.
		  
		  IsDBConnected = False
		  DB = New SQLiteDatabase
		  DB.DatabaseFile = location
		  
		  If Not location.Exists Then
		    If DB.CreateDatabaseFile Then
		      // Execute SQL to create Quote table
		      DB.SQLExecute(kQuoteTableSQL)
		      Dim errMsg As String = DB.ErrorMessage
		      IsDBConnected = True
		    End If
		  Else
		    If DB.Connect Then
		      IsDBConnected = True
		    End If
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetDBFile() As FolderItem
		  // Storing DB file in standard System location
		  #If TargetXojoCloud Then
		    Dim dbFolder As FolderItem = SpecialFolder.Documents.Child("QuoteService")
		  #Else
		    Dim dbFolder As FolderItem = SpecialFolder.ApplicationData.Child("QuoteService")
		  #EndIf
		  
		  If Not dbFolder.Exists Then
		    dbFolder.CreateAsFolder
		  End If
		  
		  Dim dbFile As FolderItem = dbFolder.Child("Quotes.sqlite")
		  
		  Return dbFile
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		DB As SQLiteDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		IsDBConnected As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mQuote() As String
	#tag EndProperty


	#tag Constant, Name = kQuoteTableSQL, Type = String, Dynamic = False, Default = \"CREATE TABLE Quote (\n  ID INTEGER NOT NULL\x2C\n  QuoteText TEXT\x2C\n  QuoteSource TEXT\x2C \n  PRIMARY KEY (ID)\n);", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="IsDBConnected"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
