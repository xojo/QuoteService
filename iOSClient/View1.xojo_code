#tag IOSView
Begin iosView View1
   BackButtonTitle =   ""
   Compatibility   =   ""
   LargeTitleMode  =   "2"
   Left            =   0
   NavigationBarVisible=   False
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   ""
   Top             =   0
   Begin iOSLabel QuoteLabel
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   QuoteLabel, 1, <Parent>, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   QuoteLabel, 2, <Parent>, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   QuoteLabel, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, *kStdControlGapV, , True
      AutoLayout      =   QuoteLabel, 8, , 0, False, +1.00, 4, 1, 343, , True
      Enabled         =   True
      Height          =   343.0
      Left            =   0
      LineBreakMode   =   "0"
      LockedInPosition=   False
      Scope           =   0
      Text            =   "Fetching a quote..."
      TextAlignment   =   "1"
      TextColor       =   &c00000000
      TextFont        =   ""
      TextSize        =   0
      Top             =   28
      Visible         =   True
      Width           =   320.0
   End
   Begin iOSButton QuoteButton
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   QuoteButton, 9, <Parent>, 9, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   QuoteButton, 7, , 0, False, +1.00, 4, 1, 100, , True
      AutoLayout      =   QuoteButton, 4, BottomLayoutGuide, 3, False, +1.00, 4, 1, -*kStdControlGapV, , True
      AutoLayout      =   QuoteButton, 8, , 0, False, +1.00, 4, 1, 30, , True
      Caption         =   "Get a Quote"
      Enabled         =   True
      Height          =   30.0
      Left            =   110
      LockedInPosition=   False
      Scope           =   0
      TextColor       =   &c007AFF00
      TextFont        =   ""
      TextSize        =   0
      Top             =   442
      Visible         =   True
      Width           =   100.0
   End
   Begin Xojo.Net.HTTPSocket QuoteSocket
      Left            =   0
      LockedInPosition=   False
      PanelIndex      =   -1
      Parent          =   ""
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   0
      ValidateCertificates=   False
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Method, Flags = &h0
		Sub GetQuote()
		  // Display a random quote using the API
		  // Change the URL as needed
		  QuoteSocket.Send("GET", "http://127.0.0.1:8080/api/quote")
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events QuoteButton
	#tag Event
		Sub Action()
		  GetQuote
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events QuoteSocket
	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  Dim jsonData As Text
		  // Convert the content returned from an API from a MemoryBlock to Text.
		  jsonData = Xojo.Core.TextEncoding.UTF8.ConvertDataToText(content)
		  
		  // Parse the JSON result into a Dictionary
		  Dim json As Xojo.Core.Dictionary
		  json = Xojo.Data.ParseJSON(jsonData)
		  
		  QuoteLabel.Text = json.Value("QuoteText") + &ua + &ua + "-- " + json.Value("QuoteSource")
		End Sub
	#tag EndEvent
	#tag Event
		Sub Error(err as RuntimeException)
		  Break
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LargeTitleMode"
		Visible=true
		Group="Behavior"
		InitialValue="2"
		Type="LargeTitleDisplayModes"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
#tag EndViewBehavior
